#ifndef LISTA_H
#define LISTA_H

#include <cstdio>
#include <cstdlib>
#include <cstring>

#define BUFFER_SIZE 101
#define WORD_SIZE 16

typedef struct node {
	char word[WORD_SIZE];
	char translation[WORD_SIZE];
	node *next;
} node;

void init(node *head);
void add_after(node *element, char word[], char translation[]);
void remove_after(node *element);
void length(node *head);
void remove_all(node *head);

//stos
void push(node *element, char word[], char translation[]);
void push_back(node *head, char word[]);
node pop(node *head);

#endif