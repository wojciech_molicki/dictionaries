#include "lista.h"

void init(node *head)
{
	head->next = NULL;	 
}

void add_after(node *element, char word[]) 
{
	node *ptr = (node*)malloc(sizeof(node));
	strcpy_s(ptr->word, WORD_SIZE, word);
	ptr->next = element->next;
	element->next = ptr;
}

void remove_after(node *element) //usuwamy element->next
{
	if(element->next != NULL) //jesli nie jest to ostatni element
	{
		node *ptr = element->next;
		element->next = element->next->next;
		free(ptr);
	}
}

void length(node *head)
{
	int len = 0;

	while(head->next != NULL)
	{
		len++;
		head = head->next;
	}
	printf("Dlugosc = %d", len);
}

void remove_all(node *head)
{
	head = head->next;
	if (head == NULL)
		return;
	while (head->next != NULL)
	{
		node *wsk = head;
		head = head->next;
		free(wsk);
	}
	free(head->next);
	head->next = NULL;
}

void push(node *head, char word[])
{
	node *ptr = (node*)malloc(sizeof(node));
	strcpy_s(ptr->word, WORD_SIZE, word);
	ptr->next = head->next;
	head->next = ptr;
}

void push_back(node *head, char word[]) 
{
	while(head->next != NULL)
		head = head->next;
	node *ptr = (node*)malloc(sizeof(node));
	strcpy_s(ptr->word, WORD_SIZE, word);
	ptr->next = head->next;
	head->next = ptr;
}

node pop(node *head)
{
	node returnNode;
	strcpy_s(returnNode.word, WORD_SIZE, head->next->word);
	//strcpy_s(returnNode.translation, WORD_SIZE, head->next->translation);
	remove_after(head);
	return returnNode;
}