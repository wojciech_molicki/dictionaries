#include <cstdio>
#include <cstdlib>
#include <cstring>

#include "BST.h"

#define BLACK false
#define RED true
#define WORD_SIZE 16

void initBST(nodeBST *node) {
	node->left = NULL;
	node->right = NULL;
}

char* searchBST(nodeBST *tree, char* word) {
	while(tree) {
		int wynik = strcmp(tree->word, word);
		if (wynik == 0)
			return tree->translation;
		if (wynik > 0)
			tree = tree->right;
		if (wynik < 0)
			tree = tree->left;
	}
	return NULL;
}

void insertBST(nodeBST *tree, char word[], char translation[]) {
	nodeBST *whereToPutIt = tree;
	//jesli root jest pusty
	if (tree->translation == NULL)	{
		strcpy_s(tree->word, WORD_SIZE, word);
		strcpy_s(tree->translation, WORD_SIZE, translation);
		return;
	}

	int wynik = 0;
	while(whereToPutIt) {
		wynik = strcmp(whereToPutIt->word, word);
		if (wynik == 0)
			break;
		if (wynik < 0) {
			if (whereToPutIt->left != NULL)
				whereToPutIt = whereToPutIt->left;
			else
				break;
		}
		else {
			if (whereToPutIt->right != NULL)
				whereToPutIt = whereToPutIt->right;
			else
				break;
		}
	}
	if (wynik != 0) {
		nodeBST *newNode = (nodeBST*)malloc(sizeof(nodeBST));
		initBST(newNode);

		//kopiowanie slowa i tlumaczenia
		strcpy_s(newNode->word, WORD_SIZE, word);
		strcpy_s(newNode->translation, WORD_SIZE, translation);

		if (wynik < 0) {
			whereToPutIt->left = newNode;
		}
		else if (wynik > 0) {
			whereToPutIt->right = newNode;
		}
	}
	else {
		strcpy_s(whereToPutIt->translation, WORD_SIZE, translation);
	}
}