#ifndef TEST_H
#define TEST_H

#include <cassert>

#include "lista.h"
#include "trie.h"

void conduct_tests(trie *t);

#endif