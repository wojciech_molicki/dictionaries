#include "hashTable.h"

int hashF(const char name[]) {
	double hashValue = 0;
	for (int i = 0; name[i] != '\0'; i++) {
		hashValue += (name[i] - 'a') * pow(1.07, i);
	}
	return HTABLE_SIZE*fmod(HTABLE_A*hashValue, 1);
}

void hTableInit(hTable *h)
{
	for (int i = 0; i < HTABLE_SIZE; i++) {
		h->table[i] = NULL;
	}			
}

void hTableInsert(hTable *h, char word[], char translation[]) {
	int i = hashF(word);
	node *pointer = h->table[i]; // head
	if (pointer == NULL)
	{
		h->table[i] = (node*)malloc(sizeof(node));
		init(h->table[i]);
		add_after(h->table[i], word, translation);
	}
	else
	{
		pointer = pointer->next;
		while(pointer != NULL) {
			if (strcmp(pointer->word, word) == 0) {
				strcpy_s(pointer->translation, WORD_SIZE, translation);
				return;
			}
			pointer = pointer->next;
		}
		add_after(h->table[i], word, translation);
	}

}

bool hTableSearch(hTable *h, char word[], char dest[]) {
	int i = hashF(word);
	if (h->table[i] == NULL)
		return false;
	node *pointer = h->table[i]->next;
	while (pointer != NULL) {
		if (strcmp(pointer->word, word) == 0) {
			strcpy_s(dest, WORD_SIZE, pointer->translation);
			return true;
		}
		pointer = pointer->next;
	}
	return false;
}