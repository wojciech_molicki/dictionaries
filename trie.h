#ifndef TRIE_H
#define TRIE_H

#include <cstdio>
#include <cstdlib>
#include <cstring>

#define LETTER_COUNT 26
#define WORD_SIZE 16

typedef struct trie {
	trie* children[LETTER_COUNT];
	char* translation;
};

void trieInit(trie *t);

void insertIntoTrie(trie *t, char word[], char translation[]);

char* getTranslation(trie *t, char word[]);



#endif