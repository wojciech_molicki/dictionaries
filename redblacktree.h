#ifndef REDBLACKTRE_H
#define REDBLACKTREE_H

#define WORD_SIZE 16

struct nodeRBT {
	char word[WORD_SIZE];
	char *translation;
	bool color;
	int nodeNumber;
	nodeRBT *left;
	nodeRBT *right;
	nodeRBT *parent;
};

// helper & utility

void initRBT(nodeRBT *node);

nodeRBT* rightRotate(nodeRBT *tree, nodeRBT *node);
nodeRBT* leftRotate(nodeRBT *tree, nodeRBT *node);


// interface

char* searchRBT(nodeRBT *tree, char* word);
void insertRBT(nodeRBT *tree, char word[], char translation[]);


#endif