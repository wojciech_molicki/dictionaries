#include "test.h"

//#define NDEBUG

void conduct_tests(trie *t) {
	insertIntoTrie(t, "wojtek", "beka");
	assert(strcmp(getTranslation(t, "wojtek"), "beka") == 0);
	insertIntoTrie(t, "adc", "beka");
	insertIntoTrie(t, "wojtek", "xxx");
	assert(strcmp(getTranslation(t, "wojtek"), "xxx") == 0);
	insertIntoTrie(t, "b", "beka");
	insertIntoTrie(t, "a", "beka");
	assert(strcmp(getTranslation(t, "a"), "beka") == 0);
	
	insertIntoTrie(t, "a", "x");
	assert(strcmp(getTranslation(t, "a"), "x") == 0);
	insertIntoTrie(t, "zxc", "beka");
	insertIntoTrie(t, "s", "beka");
	insertIntoTrie(t, "xxxxxxxxxxxxxxx", "beka");
	insertIntoTrie(t, "abcdefghijklmno", "beka");
	insertIntoTrie(t, "c", "c");
	
	assert(strcmp(getTranslation(t, "b"), "beka") == 0);
	assert(strcmp(getTranslation(t, "adc"), "beka") == 0);
	assert(strcmp(getTranslation(t, "zxc"), "beka") == 0);
	assert(strcmp(getTranslation(t, "s"), "beka") == 0);
	assert(strcmp(getTranslation(t, "xxxxxxxxxxxxxxx"), "beka") == 0);
	assert(strcmp(getTranslation(t, "abcdefghijklmno"), "beka") == 0);
	assert(strcmp(getTranslation(t, "c"), "c") == 0);
	assert(getTranslation(t, "csccscasc") == NULL);
	assert(getTranslation(t, "ba") == NULL);
	assert(getTranslation(t, "ab") == NULL);
	assert(getTranslation(t, "d") == NULL);
	assert(getTranslation(t, "xxxxxxxxxxxxxy") == NULL);
}