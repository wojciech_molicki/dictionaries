#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>

#include "lista.h"
#include "trie.h"
#include "redblacktree.h"
#include "test.h"

#define WORD_SIZE 16

void wczytaj_slownik(trie *t) {
	char znak;
	char buforSlowo[WORD_SIZE], buforTlumaczenie[WORD_SIZE];
	bool wczytujSlowo = true;
	int i = 0, j = 0;
	while (znak = getchar()) {		
		if (znak == EOF)
			break;
		if (znak >= 'A' && znak <= 'Z') {
			ungetc(znak, stdin);
			break;
		}
		if (znak == ' ') {
			wczytujSlowo = false;
			continue;
		}
		if (znak == '\n') {
			wczytujSlowo = true;
			buforSlowo[i] = '\0';
			buforTlumaczenie[j] = '\0';
			insertIntoTrie(t, buforSlowo, buforTlumaczenie);
			i = 0; j = 0;
			continue;
		}
		if (wczytujSlowo) {
			buforSlowo[i] = znak;
			i++;
		}
		else {
			buforTlumaczenie[j] = znak;
			j++;
		}
	}
}

//wczytuje zdanie do listy
void wczytaj_zdanie(node *z) {
	char znak;
	int i = 0;
	bool nowe_zdanie = false;
	char bufor[WORD_SIZE];
	while (znak = getchar()) {
		if (znak == ' ' && i != 0) {
			bufor[i] = '\0';
			i = 0;
			push_back(z, bufor);
			z = z->next;
			continue;
		}	
		if (znak == '\n')
		{
			//bufor[i] = '\0';
			//push_back(z, bufor);
			bufor[0] = znak;
			bufor[1] = '\0';
			push_back(z, bufor);
			return;
		}
		
		if (znak == EOF)
			return;

		if (znak != ' ')
			bufor[i++] = znak;
	}
}

void zamien_na_male_i_usun_znaki(char slowo[], char dest[]) {
	int i = 0, j = 0;
	while(slowo[i] != '\0') {
		if (slowo[i] >= 'a' && slowo[i] <= 'z') {
			dest[j] = slowo[i];
			j++;
			i++;
			continue;
		}
		if (slowo[i] <= 'Z' && slowo[i] >= 'A') {
			dest[j] = slowo[i] + 32;
			j++;
			i++;
			continue;
		}
		i++;
	}
	dest[j] = '\0';
}

void tlumacz_zdania(trie *t, node *zdanie) {
	char buforWydruku[WORD_SIZE];
	char buforSlowa[WORD_SIZE];

	bool daSiePrzetlumaczyc = true;
	bool wypisanoBrakSlow = false;

	
	zdanie = zdanie->next;
	node *zdanie_poczatek = zdanie;

	while (zdanie != NULL) {
		// tlumacz liste, jesli napotkasz problem wypisz jednokrotnie "Brak slow: "
		// a potem slowa ktorych nie ma w slowniku
		if (zdanie->word[0] == '-')
		{
			zdanie = zdanie->next;
			continue;
		}
		if (zdanie->word[0] == '\n')
		{
			zdanie = zdanie->next;
			continue;
		}
		zamien_na_male_i_usun_znaki(zdanie->word, buforSlowa);
		char *tlumaczenie = getTranslation(t, buforSlowa);

		if (tlumaczenie != NULL) {
			strcpy_s(zdanie->translation, WORD_SIZE, tlumaczenie);
		}
		else {
			if (wypisanoBrakSlow == false) {
				printf("Brak slow: ");
				wypisanoBrakSlow = true;
			}

			printf("%s ", buforSlowa);
		}
		zdanie = zdanie->next;
	}

	if (wypisanoBrakSlow == true)
	{
		printf("\n");
		return;
	}

	while (zdanie_poczatek->next != NULL) {
		// wypisz przetlumaczona liste zgodnie z oryginalem

		if (zdanie_poczatek->word[0] == '-') {
			printf("%c ", zdanie_poczatek->word[0]);
			zdanie_poczatek = zdanie_poczatek->next;
			continue;
		}
		if (zdanie_poczatek->word[0] == '\n') {
			printf("%c", zdanie_poczatek->word[0]);
			zdanie_poczatek = zdanie_poczatek->next;
			continue;
		}

		int j = 0;
		if (zdanie_poczatek->word[0] <= 'Z' && 	zdanie_poczatek->word[0] >= 'A') {
			buforWydruku[0] = zdanie_poczatek->translation[0] - 32;
			j++;
		}

		while(zdanie_poczatek->translation[j] != '\0') {
			buforWydruku[j] = zdanie_poczatek->translation[j];
			j++;
		}

		for (int i = 1;  zdanie_poczatek->word[i] != '\0'; i++) {
			if ( zdanie_poczatek->word[i] >= 'a' && zdanie_poczatek->word[i] <= 'z')
				continue;
			else {
				buforWydruku[j] = zdanie_poczatek->word[i];
				j++;
			}
		}
		buforWydruku[j] = '\0';
		printf("%s ", buforWydruku);

		node *ptr = zdanie_poczatek;
		zdanie_poczatek = zdanie_poczatek->next;
		free(ptr);
	}
	//printf("\n");
}

int main() 
{
	trie tr;
	trie* t = &tr;
	trieInit(t);

	//conduct_tests(t);

	//mozna wczytac drugi slownik
	char znak = ' ';
	
	node *z = NULL;
	while (znak != EOF) {
		znak = getchar();
		if (znak >= 'A' && znak <= 'Z') {
			ungetc(znak, stdin);
			node zdanie;
			z = &zdanie;
			init(z);
			wczytaj_zdanie(z);
			tlumacz_zdania(t, z);
			//remove_all(z);
			if (z->next == NULL)
				break;
		}
		if (znak >= 'a' && znak <= 'z') {
			ungetc(znak, stdin);
			wczytaj_slownik(t);
		}
		
	}
	//conduct_tests(t);

	return 0;
}

