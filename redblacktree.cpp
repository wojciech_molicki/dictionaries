#include <cstdio>
#include <cstdlib>
#include <cstring>

#include "redblacktree.h"

#define BLACK false
#define RED true
#define WORD_SIZE 16

/*******************************************
//	 implementacja drzewa czerwono-czarnego
//	 zaczerpnieta z wykladow
//					dr inz K.Manuszewskiego
*******************************************/

void initRBT(nodeRBT *node) {
	node->translation[0] = '\0';
	node->word[0] = '\0';
	node->color = RED;
	node->left = NULL;
	node->right = NULL;
	node->parent = NULL;
	node->nodeNumber = 0;
}

bool getColor(nodeRBT *node) {
	if (node != NULL)
		return node->color;
	return BLACK;
}

nodeRBT* rightRotate(nodeRBT *tree, nodeRBT *x) {
	nodeRBT* y = x->left;

	if (y == NULL) return tree;

	x->left = y->right;

	if( y->right != NULL)
		y->right->parent = x;

	y->parent = x->parent;

	if (x->parent == NULL)
		tree = y;
	else if ( x == x->parent->left ) // jesli jest lewym dzieckiem rodzica
		x->parent->left = y;
	else
		x->parent->right = y;

	y->right = x;
	x->parent = y;

	return tree;
}
nodeRBT* leftRotate(nodeRBT *tree, nodeRBT *x) {
	nodeRBT *y = x->right;

	if (y == NULL) return tree;

	x->right = y->left;

	if (y->left != NULL)
		y->left->parent = x;

	y->parent = x->parent;

	if (x->parent == NULL)
		tree = y;
	else if ( x == x->parent->left )
		x->parent->left = y;
	else
		x->parent->right = y;
	y->left = x;
	x->parent = y;
	return tree;
}

char* searchRBT(nodeRBT *tree, char* word) {
	while(tree) {
		int wynik = strcmp(word, tree->word);
		if (wynik == 0)
			return tree->translation;
		if (wynik > 0)
			tree = tree->right;
		if (wynik < 0)
			tree = tree->left;
	}
	return NULL;
}

nodeRBT* insertAtCorrectPlace(nodeRBT *tree, char word[], char translation[]) {
	nodeRBT *whereToPutIt = tree;
	int wynik = 0;

	if (tree->word[0] == NULL) {
		//kopiowanie pierwszego slowa
		strcpy_s(tree->word, WORD_SIZE, word);
		strcpy_s(tree->translation, WORD_SIZE, translation);
		tree->color = BLACK;
		return tree;
	}

	while(whereToPutIt) {
		wynik = strcmp(word, whereToPutIt->word);
		if (wynik == 0)
			break;
		if (wynik < 0) {
			if (whereToPutIt->left != NULL)
				whereToPutIt = whereToPutIt->left;
			else
				break;
		}
		else {
			if (whereToPutIt->right != NULL)
				whereToPutIt = whereToPutIt->right;
			else
				break;
		}
	}
	if (wynik != 0) {
		// wstawiam zawsze czerwony wezel
		nodeRBT *newNode = (nodeRBT*)malloc(sizeof(nodeRBT));
		initRBT(newNode);
		newNode->parent = whereToPutIt;

		//kopiowanie slowa i tlumaczenia
		strcpy_s(newNode->word, WORD_SIZE, word);
		strcpy_s(newNode->translation, WORD_SIZE, translation);
		if (wynik < 0) {
			whereToPutIt->left = newNode;
		}
		else if (wynik > 0) {
			whereToPutIt->right = newNode;
		}
	}
	else {
		strcpy_s(whereToPutIt->translation, WORD_SIZE, translation);
		return whereToPutIt;
	}

	return (wynik > 0) ? whereToPutIt->right : whereToPutIt->left;
}

nodeRBT* insertRBT(nodeRBT *root, char word[], char translation[]) {
	nodeRBT* x = insertAtCorrectPlace(root, word, translation);

	//root musi byc black !

	while (x != root && getColor(x->parent) == RED) {
		if (x->parent == x->parent->parent->left) {
			// porzadkowanie dla ojca po lewej

			nodeRBT* uncle = x->parent->parent->right;
			if (getColor(uncle) == RED) {
				x->parent->color = BLACK;
				uncle->color = BLACK;
				x->parent->parent->color = RED;
				x = x->parent->parent;
			}
			else {
				if (x == x->parent->right) {
					x = x->parent;
					root = leftRotate(root, x);
				}
				x->parent->color = BLACK;
				x->parent->color = RED;
				root = rightRotate(root, x->parent->parent);
			}
		}
		else {
			// porzadkowanie dla ojca po prawej

			nodeRBT* uncle = x->parent->parent->left;
			if (getColor(uncle) == RED) {
				x->parent->color = BLACK;
				uncle->color = BLACK;
				x->parent->parent->color = RED;
				x = x->parent->parent;
			}
			else {
				if (x == x->parent->left) {
					x = x->parent;
					root = rightRotate(root, x);
				}
				x->parent->color = BLACK;
				x->parent->parent->color = RED;
				root = leftRotate(root, x->parent->parent);
			}
		}
		root->color = BLACK;
	}
	return root;
}