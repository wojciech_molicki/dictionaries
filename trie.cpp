#include "trie.h"

void trieInit(trie *t) {
	for (int i = 0; i < LETTER_COUNT; i++) {
		t->children[i] = NULL;
	}
	t->translation = NULL;
}

void insertIntoTrie(trie *t, char word[], char translation[]) {
	//t is not null
	 for (int i = 0; word[i] != '\0'; i++) {
		 if (t->children[word[i] - 'a'] == NULL) {
			 t->children[word[i] - 'a'] = (trie*)malloc(sizeof(trie));
			 trieInit(t->children[word[i] - 'a']);
		 }
		 t = t->children[word[i] - 'a'];
	 }
	 if (t->translation == NULL)
		t->translation = (char*)malloc(WORD_SIZE);

	 strcpy_s(t->translation, WORD_SIZE, translation);
}

char* getTranslation(trie *t, char word[]) {

	//returns pointer to translation stored in trie, NULL for no translation
	for (int i = 0; word[i] != '\0'; i++) {
		t = t->children[word[i] - 'a'];
		if (t == NULL)
			return NULL;
	}
	return t->translation;
}